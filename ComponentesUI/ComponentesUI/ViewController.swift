//
//  ViewController.swift
//  ComponentesUI
//
//  Created by Jesus Maqueda on 01/03/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    
    var isChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        label1.text = "Mi nuevo Label"
        
        myButton.setTitle("Nuevo titulo", for: .normal)
        myButton.backgroundColor = .yellow
    }

//    @IBAction func actionButton(_ sender: UIButton) {
//        print("Primer Botón")
//    }
//
//    @IBAction func actionButton2(_ sender: UIButton) {
//        print("Segundo Botón")
//    }
//
//    @IBAction func actionButton3() {
//        print("Tercer Botón")
//    }
    
    @IBAction func actionMore(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            label1.text = "Sumar"
        case 1:
            label1.text = "Restar"
        case 2:
            label1.text = "Multiplicar"
        default:
            print("Nada")
        }
    }
    
    @IBAction func changeImageAction() {
        isChanged = !isChanged
        if isChanged {
            contentImage.image = UIImage(named: "smash")
        } else {
            contentImage.image = UIImage(named: "kartImage")
        }
    }
    
    @IBAction func changeAlpha(_ sender: UISlider) {
        contentImage.alpha = CGFloat(sender.value)
    }
    
    @IBAction func sendNameAction() {
        if let name = nameField.text?.trimmingCharacters(in: CharacterSet.whitespaces), !name.isEmpty {
            label1.text = name
        }
    }
    
}

