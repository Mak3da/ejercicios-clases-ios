//
//  Persona.swift
//  Demo1
//
//  Created by Jesus Maqueda on 26/02/21.
//
/**
 PODEMOS TENER EN UN MISMO ARCHIVO CLASES COMO QUERAMOS, YA QUE
 NO ESTAMOS OBLIGADOS COMO EN OTROS LENJUAGES, QUE LA CLASE DEBE
 TENER EL MISMO NOBRE DEL ARCHIVO, Y SOLO ESTAR ENFOCADA EN UNA
 IMPLEMENTACIÓN
 */

//SINTAXIS DE COMO CREAR UNA CLASE HEREDANDO DE OTRA CLASE CON NOMBRE "Vendedor"
class Persona:Vendedor {
    
    //PROPIEDADES
    var firstName: String?
    var lastName: String?
    var age:Int
    
    //INICIALIZADOR O COMO EN OTROS LENJUAGES, CONSTRUCTOR
    init(age:Int) {
        self.age = age
    }
    
    //METODO PUBLICO DE LA CLASE
    func validateName() {
        //TODO: Implementar funionalidad
        print("Se va a validar el nombre del usuario")
    }
    
    //MÉTODO PÚBLICO CON PASO DE PARAMETROS
    func validateName(nombre: String) -> Bool {
        //VALIDAR SI ES CORRECTO
        return true
    }
    
    //SOBRE ESCRIBIMOS EL METODO ORIGINAL QUE HEREDAMOS DE "Vendedor"
    override func venderJuegos() {
        print("No puede vender juegos, no tiene")
    }
}

//UNA SEGUNDA CLASE
class Vendedor {
    
    
    func venderJuegos() {
        print("Tiene la posibilidad de vender Juegos")
    }
}

//UNA TERCERA CLASE
class Comprador {
    
}

//UNA SINTAXIS DE CREACIÓN DE UNA ESTRUCTURA, MUY SIMILAR A LA CREACIÓN DE UNA CLASE
struct Point {
    var x: Int
    var y: Int
    
    init(x:Int, y: Int) {
        self.x = x
        self.y = y
    }
}
