//
//  ViewController.swift
//  Demo1
//
//  Created by Jesus Maqueda on 23/02/21.
//

import UIKit

class ViewController: UIViewController {
    
    let constante = 5
    var numero:Int?
    
    enum Datos {
        case uno
        case dos
        case tres
    }

    @IBOutlet weak var boton_1: UIButton!
    
    override func viewDidLoad() {
        //primero
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        instanciarPersona()
    }
    
    func instanciarPersona() {
        //CREAMOS UNA NUEVA INSTANCIA POS CLASE
        let persona = Persona(age: 38)
        persona.firstName = "Josue"
        persona.validateName()
        let _ = persona.validateName(nombre: "Alan")
        persona.venderJuegos()
        
        //SE CREA UN INSTANCIA POR ESTRUCTURA
        var point1 = Point(x: 0, y: 0)
        var point2 = point1
        point1.x = 10
        
        //SE COMPRUEBA COMO SE PASA POR VALOR, LO QUE EFECTUA UNA COPIA DE SI
        print(point1)
        print(point2)
        
        let nuevaPersona = persona
        persona.firstName = "Fernando"
        
        //SE CCOMPRUEBA QUE SE PARA POR REFERENCIA, APUNTAN A LA MISMA UBICACIÓN DE MEMORIA
        print(persona.firstName)
        print(nuevaPersona.firstName)
        
    }
    
    func parte1() {
        //numero = 10
        //1.Enlace Opcional
        if let desempaquetado1 = numero {
            print(desempaquetado1)
        } else {
            print("No tiene valor")
        }
        
        //2. Control de flujo (guard)
        guard let desempaquetado2 = numero else {
            print("No tiene Valor")
            return
        }
        //EJEMPLO CUANDO SE VALIDA CORRECTAMENTE EL GUARD SE LLAMA UNA FUNCIÓN O CUALQUIER
        //IMPLEMENTACIÒN
        loadRequestServer()
        
        //print(desempaquetado2)
        
        //3. Operador Ternario
        let desempaquetado3 = numero != nil ? numero! : 50
        print(desempaquetado3)
        
        //4. Operador de coalescencia nula
        let desempaquetado4 = numero ?? 40
        print(desempaquetado4)
        
        //5. Desempaquetado forzado
        let desempaquetadoPeligroso = numero! //Nunca usarlo
        print(desempaquetadoPeligroso)
        
        var variable = 5
        let constante = 10
        
        //Tipos de datos Básicos
        //Tipo de dato String
        let var1:String = "Esto es un string"
        //Tipo de dato Int
        let var2:Int = 5
        //Tipo de dato UInt, sin negativos
        let var3:UInt = 10
        //Tipo de dato Float
        let var4:Float = 5.5 // Solo admite hasta 32 bits, 6 digitos decimales de precisión
        //Tipo de dato Double
        let var5:Double = 10.5 // Admite hasta 64 bits, 15 digitos decimales de precisión
        // Tipo de dato Bool
        let var6:Bool = true
        
        //Operadores Básicos
        
        //De asignación (=)
        
        //Aritmeticos
        //a + b
        //b - c
        //c * a
        //a / b
        
        //COMPARATIVOS
        //a == b
        //b === a
        //a != b
        //a > b
        //b < a
        //a >= b
        //b <= a
        
        //LÓGICOS
        //a != b
        //a && b
        //a || b
        
        //CONDICIONALES
        let a = 5
        let b = 10
        //IF-ELSE
        if (a != b) {
            print("a es diferente")
        } else {
            print("son iguales")
        }
        
        //SWITCH
        switch a {
        case 5:
            print("Es cinco")
        default:
            print("Es cualquiere otro valor")
        }
        
        //SWITCH CON CONDICIONAL
        let valor = 10
        switch valor {
        case let x where x > 2:
            print("Es mayor a 2")
        case let x where x < 2:
            print("Es menor a 2")
        default:
            print("Es 2")
        }
        
        let dato = Datos.uno
        
        //IMPLEMENTACIONES DE SWITCH
        switch dato {
        case .dos, .tres, .uno:
            break
        default:
            print("Default")
        }
        
        //OPERADORES DE RANGO
        // 3...10
        // 3..<10
        
        //ITERADORES
        //for-in
        
        //OPERADOR DE RANGO QUE VA DEL 1 AL 10 INCLUYENDO EL 10
        for indice in 1...10 {
            print(indice)
        }
        
        //OPERADOR DE RANGO QUE VA DEL 1 AL 10 SIN INCLUIR EL 10
        for indice in 1..<10 {
            print(indice)
        }
        
        //HACE LA ITERACIÓN A LA INVERSA, COMIENZA DESDE 1 EL 10 AL 1
        for indice in (1...10).reversed() {
            print(indice)
        }
        
        //CON CONDICIONAL, PODEMOS INCLUIR CONDICION QUE DEBE EJECUTAR LA ITERACIÓN
        for indice in 1...10 where indice % 2 == 0 {
            
        }
        
        //NO SECUENCIAL, SE LE INDICA DE DONDE INICIA Y HASTA DONDE VA Y CUANTOS SALTOS PUEDE DAR EN LA ITERACIÓN
        for indice in stride(from: 1, to: 10, by: 3) {
            print(indice)
        }
        
        //while
        var valor2 = 1
        while valor2 < 10 {
            print(valor2)
            valor2 += 1
        }
        
        //REPEAT WHILE, LO QUE VENDRÍA SIENDO EL DO WHILE EN OTROS LENGUAJES
        var valor3 = 1
        repeat {
            print(valor3)
            valor3 += 1
        } while valor3 < 10
        
        //ARRAY O MATRIZ
        var matriz1 = [String]()
        var matriz2 = Array<String>()
        var matriz3:[String] = []
        var matriz4:[String] = ["uno", "dos", "tres"]
        matriz4.append("cuatro")
        matriz4.insert("cinco", at: 2)
        matriz4.popLast()
        matriz4.remove(at: 2)
        matriz4.count
        matriz4.last
        matriz4.first
        matriz4.removeAll()
        
        // ARREGLO INMUTABLE
        let arreglo: [Int] = [1,2,3,4,5,6]
        
        //SET, no permite valores repetidos
        var setExample = Set<Int>()
        setExample.insert(1)
    }
    
    func parte2() {
        let arreglo: [Int] = [1,2,3,4,5,6]
        //DICCIONARIOS, ES UNA LISTA DESORDENADA QUE NO CUENTA CON INDICE
        var diccionario:[String:Int] = [:]
        diccionario["llave1"] = 10
        diccionario["llave2"] = 20
        
        //print("El valor de diccionario es: \(diccionario["llave1"])")
        
        //UNA FORMA DE ITERACIÓN A UN DICCIONARIO
        for item in diccionario {
            print("El valor de la llave \(item.key) es: \(item.value)")
        }
        
        //SE ITERA EL DICCIONARIO, Y PODEMOS ASIGNARLE UN INDICE, PARA SABER EN CUAL VAMOS, NO GARANTIZA QUE SIEMPRE SEA EL MISMO ORDEN
        for (indice, value) in diccionario.enumerated() {
            print("\(indice) - \(value.key) - \(value.value)")
        }
        
        //FORMA DE COMO ITERAR A UN ARREGLO DE ENTEROS
        for valor in arreglo {
            print(valor)
        }
        
        let arregloString: [String] = ["Hola", " como estan", " hoy en este viernes"]
        
        //MAP, APLICA LO QUE ESTA EN EL CLOSURE A CADA ELEMENTO EN EL ARREGLO Y REGRESA UN NUEVO ARREGLO
        var map = arreglo.map { return $0 * 2 }
        print(map)
        
        //FILTER, DE ACUERDO A UNA CONDICION ES QUE COMO NOS FILTRARA EL CONTENIDO DEL ARREGLO EN UNO NUEVO
        var filtrado = arreglo.filter { $0 % 2 == 0 }
        print(filtrado)
        
        //REDUCE, REDUCE EL TOTAL DE ELEMENTOS EN UN ARRAY A SU MINIMA EXPRESIÓN
        var reduce = arreglo.reduce(4, { $0 + $1 })
        print(reduce)
        
        var reduceString = arregloString.reduce("Saludo: ", { $0 + $1 })
        print(reduceString)
        //
        //TUPLAS, UN TIPO DE DATO COMPUESTO
        //TUPLA CON INDICE
        var tupla1:(String, Int, Double) = ("Hola", 98, 3.1416)
        print(tupla1.2)
        
        //TUPLA CON LLAVE
        var tupla:(saludo:String, peso: Int, pi: Double) = (saludo: "Hola", peso: 80, pi: 3.1416)
        print(tupla.saludo)
        tupla.saludo = "Nuevo saludo"
        print(tupla.saludo)
        
        //LA DIFERENCIA PRINCIPAL ENTRE UNA TUPLA Y LOS ARREGLOS Y DICCIONARIOS, ES QUE ÉSTA NO ES UNA COLECCIÓN
        // Y LA DIFERENCA CLAVE ES QUE SUS VALORES ALMACENADOS, NO NECESITAN SER DEL MISMO TIPO.
    }
    
    //EJEMPLO DE UNA FUNCIÓN SENCILLA
    func nombreFuncion() {
        
    }
    
    //EJEMPLO DE FUNCIÓN CON PASO DE PARAMETROS Y CON RETORNO DE UN VALOR (TUPLA)
    func funcionParams(param1: String, param2:String, param3:Int) -> (String, Float) {
        //Cuerpo de la funcion, lo que tenga que realizar
        return ("Respuesta", 3.8)
    }
    
    //EJEMPLO DE FUNCIÓN CON RETORNO DE VALOR
    func retornoNormal() -> Int {
        return 10
    }
    
    func retornoTupla() -> (Int, String, Float) {
        return (10,"Hola", 3.16)
    }
    
    func loadRequestServer() {
        print("Ir al servidor")
    }
    
    //MÉTODO QUE SE EJECUTA AL SER CARGADA LA VISTA, DESPUÉS DEL "viewDidLoad()", ESTA VISIBLE LA VISTA
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MÉTODO QUE SE EJECUTA CUANDO LA VISTA ACTUAL AH DESAPARECIDO, FUERA DE LA PANTALLA
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MÉTODO QUE SE EJECUTA ANTES DEL "viewDidDisappear()", ANTES DE SER MOSTRADA LA VISTA COMPLETAMENTE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MÉTODO QUE SE EJECUTA ANTES DEL "viewDidDisappear()", ANTES DE ESTAR FUERA COMPLETAMENTE DE LA VISTA
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}

