//
//  ViewController.swift
//  Carrusel
//
//  Created by Jesus Maqueda on 03/03/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var contentScrollView: UIView!
    
    let listadoImagenes: [String] = ["Imagen 1", "Imagen 2", "Imagen 3","Imagen 1", "Imagen 2", "Imagen 3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cargarElementosCarrusel()
    }

    func cargarElementosCarrusel() {
        var listImagenes: [ImageViewGallery] = []
        for (i, item) in listadoImagenes.enumerated() {
            let image = ImageViewGallery()
            image.translatesAutoresizingMaskIntoConstraints = false
            self.contentScrollView.addSubview(image)
            if i == 0 {
                let leading:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .leading, relatedBy: .equal, toItem: contentScrollView, attribute: .leading, multiplier: 1, constant: 0)
                let top:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: contentScrollView, attribute: .top, multiplier: 1, constant: 0)
                let bottom:NSLayoutConstraint = NSLayoutConstraint(item: contentScrollView!, attribute: .bottom, relatedBy: .equal, toItem: image, attribute: .bottom, multiplier: 1, constant: 0)
                let width:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 140)
                self.view.addConstraints([leading, top, bottom, width])
            } else if i > 0 && i < (listadoImagenes.count - 1) {
                let oldImage = listImagenes[i-1]
                let leading:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .leading, relatedBy: .equal, toItem: oldImage, attribute: .trailing, multiplier: 1, constant: 10)
                let top:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: contentScrollView, attribute: .top, multiplier: 1, constant: 0)
                let bottom:NSLayoutConstraint = NSLayoutConstraint(item: contentScrollView!, attribute: .bottom, relatedBy: .equal, toItem: image, attribute: .bottom, multiplier: 1, constant: 0)
                let width:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 140)
                self.view.addConstraints([leading, top, bottom, width])
            } else {
                let oldImage = listImagenes[i-1]
                let leading:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .leading, relatedBy: .equal, toItem: oldImage, attribute: .trailing, multiplier: 1, constant: 10)
                let top:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: contentScrollView, attribute: .top, multiplier: 1, constant: 0)
                let trailing: NSLayoutConstraint = NSLayoutConstraint(item: contentScrollView!, attribute: .trailing, relatedBy: .equal, toItem: image, attribute: .trailing, multiplier: 1, constant: 10)
                let bottom:NSLayoutConstraint = NSLayoutConstraint(item: contentScrollView!, attribute: .bottom, relatedBy: .equal, toItem: image, attribute: .bottom, multiplier: 1, constant: 0)
                let width:NSLayoutConstraint = NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 140)
                self.view.addConstraints([leading, top, trailing, bottom, width])
            }
            listImagenes.append(image)
        }
        //self.view.layoutIfNeeded()
        
    }

}

