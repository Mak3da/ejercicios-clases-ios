//
//  HomeViewController.swift
//  MultivistaApp
//
//  Created by Jesus Maqueda on 05/03/21.
//

import UIKit

class HomeViewController: UIViewController {
    var data: [String:String]?
    @IBOutlet weak var saludoLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        validateData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("Desaparesiste")
    }
    
    private func validateData() {
        if let data = data, let user = data["user"] {
            saludoLbl.text = "Hola \(user), biendenido al Home"
        }
    }
    
    @IBAction func closeSession() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
