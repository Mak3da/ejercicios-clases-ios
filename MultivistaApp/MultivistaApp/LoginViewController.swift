//
//  LoginViewController.swift
//  MultivistaApp
//
//  Created by Jesus Maqueda on 05/03/21.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var userField: UITextField!
    @IBOutlet weak var passField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cleanFields() {
        userField.text = ""
        passField.text = ""
    }
    
    @IBAction func validateButton() {
        var dict:[String:String] = [:]
        if let user = userField.text, !user.isEmpty {
            if let pass = passField.text, !pass.isEmpty {
                dict["user"] = user
                dict["pass"] = pass
                performSegue(withIdentifier: "homeSegue", sender: dict)
            } else {
                mostrarAlertaError(message: "Introduce contraseña")
            }
        } else {
            mostrarAlertaError(message: "Introduce usuario")
        }
    }
    
    func mostrarAlertaError(message: String) {
        let alerta = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        alerta.addAction(acceptAction)
        present(alerta, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "homeSegue" {
            if let vc = segue.destination as? HomeViewController, let data = sender as? [String:String] {
                vc.data = data
            }
            cleanFields()
        }
    }
}
